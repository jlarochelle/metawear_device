from mbientlab.metawear import *
from threading import Event
from time import gmtime, strftime
import numpy as np

class DataHandler:
    def __init__(self, signal):
        self.identifier = libmetawear.mbl_mw_anonymous_datasignal_get_identifier(signal)
        self.data_handler_fn = FnVoid_VoidP_DataP(self.data_logger_download_callback)
        self.file_object = None
        self.data_array = []

    def save_to_file(self):
        current_time_string = strftime("%Y%m%d_%H%M%S", gmtime())
        actual_file_path = file_path + "\\" + current_time_string + ".npy"

        np.save(actual_file_path, np.asarray(self.data_array))

        return actual_file_path

    def data_logger_download_callback(self, context, data):
        parsed_data = parse_value(data)
        self.data_array.append((data.contents.epoch,
                                parsed_data.x,
                                parsed_data.y,
                                parsed_data.z))

result = {}

def data_signal_callback(context, board, signals, size):
    result['length'] = size
    result['signals'] = cast(signals, POINTER(c_void_p * size)) if signals is not None else None
    e.set()

data_signal_callback_fn = FnVoid_VoidP_VoidP_VoidP_UInt(data_signal_callback)

def get_anonymous_signals():
    libmetawear.mbl_mw_metawearboard_create_anonymous_datasignals(device.board, None, data_signal_callback_fn)
    e.wait()
    e.clear()

    return result


def progress_update_handler(ctx, left, total):
    if total is not 0:
        percent = (total - left) / total * 100
        print(str(left) + " left out of " + str(total) + " (" + str(percent) + "% done)")
        if (left == 0):
            e.set()


def unknown_entry_handler(ctx, id, epoch, data, length):
    print("unknown entry = " + str(id))


progress_update_fn = FnVoid_VoidP_UInt_UInt(progress_update_handler)
unknown_entry_fn = FnVoid_VoidP_UByte_Long_UByteP_UByte(unknown_entry_handler)

"""
o2stat prototype data reader

Connects to a logging metamotionR board, stops logging and saves data in .txt file.
"""
# User params
metamotionr_address = "D0:27:64:70:21:4E"
clearDeviceAfterwards = True # Do we want to clear the device after reading it?
file_path = r"C:\Users\rd_icentia\Documents\larochellej\prototype o2stat\accelero_misc"
expected_identifier = b'acceleration' # Output of programmer script

print('Running O2STAT prototype data reader.')
# Connecting to device
address = metamotionr_address
device = MetaWear(address)
device.connect()
print("Connected to device " + address + ".")
libmetawear.mbl_mw_settings_set_connection_parameters(device.board, 7.5, 7.5, 0, 6000)

# Event() object is used to wait for specific functions before continuing code execution.
e = Event()
e.clear()

# Creating anonymous signals
print("Creating anonymous signals.")
anonymous_signals = get_anonymous_signals()

if anonymous_signals['signals'] is None:
    if anonymous_signals['length'] != 0:
        print("Error creating anonymous signals, status = " + str(anonymous_signals['length']))
    else:
        print("No active loggers detected")
else:
    print(str(anonymous_signals['length']) + " active logger(s) detected.")

    download_input = input("Stop logging and download logs? (y/n)")

    if download_input == 'y':

        # Stop active loggers
        libmetawear.mbl_mw_logging_stop(device.board)
        libmetawear.mbl_mw_acc_stop(device.board)
        libmetawear.mbl_mw_acc_disable_acceleration_sampling(device.board)
        print("Active loggers stopped.")

        handlers = []
        for x in range(anonymous_signals['length']):
            wrapper = DataHandler(anonymous_signals['signals'].contents[x])


            if wrapper.identifier == expected_identifier: # We have expected signal.
                print("Found expected signal.")
                libmetawear.mbl_mw_anonymous_datasignal_subscribe(anonymous_signals['signals'].contents[x],
                                                                  None,
                                                                  wrapper.data_handler_fn)
                handlers.append(wrapper)

        print("Downloading log")

        if len(handlers) > 0:

            download_handler = LogDownloadHandler(context=None, received_progress_update=progress_update_fn,
                                                  received_unknown_entry=unknown_entry_fn,
                                                  received_unhandled_entry=cast(None, FnVoid_VoidP_DataP))
            libmetawear.mbl_mw_logging_download(device.board, 1000, byref(download_handler))
            e.wait()
            saved_file_path = handlers[0].save_to_file()

            print("Download completed")
            print("Data stored in file " + saved_file_path)

            # Remove logger
            logger_id = 0
            logger = libmetawear.mbl_mw_logger_lookup_id(device.board, logger_id)
            libmetawear.mbl_mw_logger_remove(logger)

        # Clear device log memory
        if clearDeviceAfterwards:
            libmetawear.mbl_mw_logging_clear_entries(device.board)
            libmetawear.mbl_mw_debug_reset_after_gc(device.board)
    else:
        print("Doing nothing on device.")

device.on_disconnect = lambda status: e.set()
libmetawear.mbl_mw_debug_disconnect(device.board)
e.wait()
print('Device disconnected.')