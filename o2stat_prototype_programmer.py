from mbientlab.metawear import *
from threading import Event


def display_accelero_type(board):
    acc_type = libmetawear.mbl_mw_metawearboard_lookup_module(board, 3)

    if acc_type == Const.MODULE_ACC_TYPE_MMA8452Q: print("Accelero = MMA8452Q")
    elif acc_type == Const.MODULE_ACC_TYPE_BMI160: print("Accelero = BMI160")
    elif acc_type == Const.MODULE_ACC_TYPE_BMA255: print("Accelero = BMA225")
    else: print("Unexpected or no accelerometer on board.")


def set_up_data_logger(device, signal):

    # Get logger for given signal
    libmetawear.mbl_mw_datasignal_log(signal, None, data_logger_callback_fn)


data_logger = None

def data_logger_callback(context, given_data_logger):
    global data_logger
    data_logger = given_data_logger
    e.set()


data_logger_callback_fn = FnVoid_VoidP_VoidP(data_logger_callback)


def set_up_accelerometer(device, sampling_freq, data_range):
    # Set sampling frequency
    libmetawear.mbl_mw_acc_set_odr(device.board, sampling_freq)
    # Set data range
    libmetawear.mbl_mw_acc_set_range(device.board, data_range)
    # Write config settings
    libmetawear.mbl_mw_acc_write_acceleration_config(device.board)

    # Get signal
    signal = libmetawear.mbl_mw_acc_get_acceleration_data_signal(device.board)

    print("Sampling freq. = " + str(sampling_freq) + " Hz")
    print("Measurement range = +-" + str(data_range) + " g")
    return signal


def led_start_blinking(device, color=LedColor.GREEN, count=Const.LED_REPEAT_INDEFINITELY):
    """
    Enable LED blinking
    :param device:
    :param color: LedColor.GREEN, RED or BLUE.
    :param count: Repeat count between 1 and 255
    :return:
    """

    pattern = LedPattern(repeat_count=count)
    libmetawear.mbl_mw_led_load_preset_pattern(byref(pattern), LedPreset.BLINK)
    libmetawear.mbl_mw_led_write_pattern(device.board, byref(pattern), color)
    libmetawear.mbl_mw_led_play(device.board)


def led_stop_blinking(device):
    libmetawear.mbl_mw_led_stop_and_clear(device.board)

"""
o2stat_prototype_programmer

Programs the MetaMotionR device to be used in O2STAT prototype.
"""
# User params
metamotionr_address = "D0:27:64:70:21:4E"
clearDevice = True # Do we want to clear the device logs?
# Accelero is BMI 1600
accelero_sampling_freq = 6.25 # Hz, allowed are listed in mbientlab.metawear/cbindings.py/AccBmi160Odr
accelero_range = 2 # +- g, allowed are listed in mbienlat.metawear/cbindings.py/AccBoschRange


print('Running O2STAT prototype programmer.')
# Connecting to device
address = metamotionr_address
device = MetaWear(address)
device.connect()
print("Connected to device " + address + ".")
libmetawear.mbl_mw_settings_set_connection_parameters(device.board, 7.5, 7.5, 0, 6000)

# Event() object is used to wait for specific functions before continuing code execution.
e = Event()
e.clear()


# Do we want to clear the device logs?
if clearDevice:
    libmetawear.mbl_mw_logging_clear_entries(device.board)
    print('Cleared device logs.')

print("Setting up accelerometer.")
display_accelero_type(device.board)
accelero_signal = set_up_accelerometer(device, accelero_sampling_freq, accelero_range)

print("Setting up data logger.")
# Often, data logger is not created on first try.
tries = 0
while (data_logger is None):
    tries += 1
    tries_str = "[Try #" + str(tries) + "]"
    set_up_data_logger(device, accelero_signal)
    e.wait()
    e.clear()

    if data_logger is None:
        print(tries_str + "Failed to create data logger, trying again.")
    else:
        print(tries_str + "Succesfully created data logger.")

# Start logging
print("Starting data logging.")
libmetawear.mbl_mw_acc_enable_acceleration_sampling(device.board)
libmetawear.mbl_mw_acc_start(device.board)
overwrite_old_entries = True
libmetawear.mbl_mw_logging_start(device.board, int(overwrite_old_entries))

# Obtain and show identifier
identifier = libmetawear.mbl_mw_logger_generate_identifier(data_logger)
id = libmetawear.mbl_mw_logger_get_id(data_logger)
print('Identifier: ' + str(identifier))
print("id: " + str(id))

# Blink led 10 times
led_stop_blinking(device)
led_start_blinking(device, LedColor.RED, 10)

print('Accelerometer logging has begun. Note down above identifier.')

device.on_disconnect = lambda status: e.set()

libmetawear.mbl_mw_debug_disconnect(device.board)
e.wait()
e.clear()

print("Disconnected from device.")


