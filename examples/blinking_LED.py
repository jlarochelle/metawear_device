from mbientlab.metawear import *
from threading import Event

"""
blinking LED

Makes LED blink upon execution."""
e = Event()
address = None

address = "D0:27:64:70:21:4E"
device = MetaWear(address)
device.connect()

pattern= LedPattern(repeat_count= Const.LED_REPEAT_INDEFINITELY)
libmetawear.mbl_mw_led_load_preset_pattern(byref(pattern), LedPreset.BLINK)
libmetawear.mbl_mw_led_write_pattern(device.board, byref(pattern), LedColor.GREEN)
libmetawear.mbl_mw_led_play(device.board)

input('Press enter to quit.')
libmetawear.mbl_mw_led_stop_and_clear(device.board)
device.disconnect()