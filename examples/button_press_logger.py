from mbientlab.metawear import *
from threading import Event
from time import sleep

"""
button_press_logger

Accelero data logging is activated for 4 seconds upon button press, then data is downloaded.
"""

address = "D0:27:64:70:21:4E"
device = MetaWear(address)
device.connect()
print("Connected to device.")
libmetawear.mbl_mw_logging_clear_entries(device.board)

e = Event()
actual_logger = None

def data_logger(a_context, a_logger):
    print("Data logger callback.")
    global actual_logger
    actual_logger = a_logger
    e.set()

data_logger_callback = FnVoid_VoidP_VoidP(data_logger)

# Set up led blinking
pattern= LedPattern(repeat_count= Const.LED_REPEAT_INDEFINITELY)
libmetawear.mbl_mw_led_load_preset_pattern(byref(pattern), LedPreset.BLINK)
libmetawear.mbl_mw_led_write_pattern(device.board, byref(pattern), LedColor.GREEN)
libmetawear.mbl_mw_led_play(device.board)

# Setup accelero
libmetawear.mbl_mw_settings_set_connection_parameters(device.board, 7.5, 7.5, 0, 6000)
libmetawear.mbl_mw_acc_set_odr(device.board, 25.0)
libmetawear.mbl_mw_acc_set_range(device.board, 16.0)
libmetawear.mbl_mw_acc_write_acceleration_config(device.board)

# Get logger
accelero_signal = libmetawear.mbl_mw_acc_get_acceleration_data_signal(device.board)
libmetawear.mbl_mw_datasignal_log(accelero_signal, None, data_logger_callback)
e.wait()
e.clear()
if actual_logger == None:
    print("Failed to create logger.")
else:
    print("Logger successfully created.")

    # Subscribe to logger
    def logger_data_handling(context, data):
        print(str(data.contents.epoch) + " - " + str(parse_value(data)))

    logger_data_handling_fn = FnVoid_VoidP_DataP(logger_data_handling)
    libmetawear.mbl_mw_logger_subscribe(actual_logger, None, logger_data_handling_fn)
    # Proceed with logging
    libmetawear.mbl_mw_acc_enable_acceleration_sampling(device.board)
    libmetawear.mbl_mw_acc_start(device.board)
    libmetawear.mbl_mw_logging_start(device.board, 1)
    sleep(4.0)
    libmetawear.mbl_mw_logging_stop(device.board)
    libmetawear.mbl_mw_acc_stop(device.board)
    libmetawear.mbl_mw_acc_disable_acceleration_sampling(device.board)

    # Download logged data
    def progress_update_handler(ctx, left, total):
        print(str(left) + " entries left (out of total " + str(total) + ")")
        if (left == 0):
            e.set()


    def unknown_entry_handler(ctx, id, epoch, data, length):
        print("unknown entry = " + str(id) + " (epoch: " + str(epoch) + ")")


    print("Downloading log")
    progress_update_fn = FnVoid_VoidP_UInt_UInt(progress_update_handler)
    unknown_entry_fn = FnVoid_VoidP_UByte_Long_UByteP_UByte(unknown_entry_handler)
    download_handler = LogDownloadHandler(context=None, received_progress_update=progress_update_fn,
                                          received_unknown_entry=unknown_entry_fn,
                                          received_unhandled_entry=cast(None, FnVoid_VoidP_DataP))

    libmetawear.mbl_mw_logging_download(device.board, 100, byref(download_handler))
    e.wait()
    e.clear()
    print("Download completed")
    libmetawear.mbl_mw_macro_erase_all(device.board)
    libmetawear.mbl_mw_debug_reset_after_gc(device.board)




input('Press enter to quit.')

libmetawear.mbl_mw_led_stop_and_clear(device.board)

device.on_disconnect = lambda status: e.set()

libmetawear.mbl_mw_debug_disconnect(device.board)
e.wait()
print("Disconnected from device.")